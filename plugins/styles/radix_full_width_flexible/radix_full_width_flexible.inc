<?php

/**
 * @file
 * Flexible style plugin.
 */

$plugin = array(
  'title' => t('Flexible'),
  'description' => t('Add full-width support.'),
  'render pane' => 'radix_full_width_flexible_render_pane',
  'render region' => 'radix_full_width_flexible_render_region',
  'settings form' => 'radix_full_width_flexible_region_settings_form',
  'settings form validate' => 'radix_full_width_flexible_settings_validate',
  'pane settings form' => 'radix_full_width_flexible_pane_settings_form',
  'pane settings form validate' => 'radix_full_width_flexible_settings_validate',

  // Signals to radix_full_width that we can work as in 'full width' mode
  // if instructed.
  'full width' => TRUE,
);

/**
 * Region settings form callback.
 */
function radix_full_width_flexible_pane_settings_form($settings) {
  $form = array();

  // Settings: Dimensions

  $form['size'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dimensions'),
    '#parents' => array('settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['size']['min_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum height'),
    '#default_value' => isset($settings['min_height']) ? $settings['min_height'] : '0',
    '#field_suffix' => 'px',
  );

  $form['size']['full_width_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Full-width pane'),
    '#description' => t('Expands the pane to the full width of the viewport, allowing content to render outside the grid system. <em>Note: This will only work if the theme, layout, and region support and are configured to render full-width content.</em>'),
    '#default_value' => isset($settings['full_width_content']) ? $settings['full_width_content'] : FALSE,
  );

  $form['size']['padding'] = array(
    '#type' => 'fieldset',
    '#title' => t('Padding'),
    '#parents' => array('settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $padding_types = array(
    'padding_top' => t('Padding top'),
    'padding_left' => t('Padding left'),
    'padding_bottom' => t('Padding bottom'),
    'padding_right' => t('Padding right'),
  );
  foreach ($padding_types as $padding_type => $padding_label) {
    $form['size']['padding'][$padding_type] = array(
      '#type' => 'textfield',
      '#title' => $padding_label,
      '#default_value' => isset($settings[$padding_type]) ? $settings[$padding_type] : '0',
      '#field_suffix' => 'px',
    );
  }

  // Settings: Parallax

  $form['parallax'] = array(
    '#type' => 'fieldset',
    '#title' => t('Parallax'),
    '#parents' => array('settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['parallax']['parallax_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable parallax background on pane'),
    '#default_value' => isset($settings['parallax_content']) ? $settings['parallax_content'] : FALSE,
  );

  $form['parallax']['image_src'] = array(
    '#type' => 'textfield',
    '#title' => t('Background image URI'),
    '#default_value' => (isset($settings['image_src'])) ? $settings['image_src'] : '',
    '#description' => t('You must provide a path to the image you wish to apply to the parallax effect.'),
    // Set this element to be required if the above parallax_content checkbox is checked.
    '#states' => array(
      // Action to take.
      'required' => array(
        ':input[name="settings[parallax_content]"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['parallax']['natural_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Natural width'),
    '#default_value' => (isset($settings['natural_width'])) ? $settings['natural_width'] : '',
    '#description' => t('You can provide the natural width and natural height of an image to speed up loading and reduce error when determining the correct aspect ratio of the image.'),
    '#field_suffix' => 'px',
  );

  $form['parallax']['natural_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Natural height'),
    '#default_value' => (isset($settings['natural_height'])) ? $settings['natural_height'] : '',
    '#description' => t('You can provide the natural width and natural height of an image to speed up loading and reduce error when determining the correct aspect ratio of the image.'),
    '#field_suffix' => 'px',
  );

  $form['parallax']['position_x'] = array(
    '#type' => 'textfield',
    '#title' => t('Position X'),
    '#default_value' => (isset($settings['position_x'])) ? $settings['position_x'] : '',
    '#description' => t('This is analogous to the background-position css property. Specify coordinates as top, bottom, right, left, center, or pixel values (e.g. -10px 0px). The parallax image will be positioned as close to these values as possible while still covering the target element.'),
  );

  $form['parallax']['position_y'] = array(
    '#type' => 'textfield',
    '#title' => t('Position Y'),
    '#default_value' => (isset($settings['position_y'])) ? $settings['position_y'] : '',
    '#description' => t('This is analogous to the background-position css property. Specify coordinates as top, bottom, right, left, center, or pixel values (e.g. -10px 0px). The parallax image will be positioned as close to these values as possible while still covering the target element.'),
  );

  $form['parallax']['speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Speed'),
    '#default_value' => (isset($settings['speed'])) ? $settings['speed'] : '',
    '#description' => t('The speed at which the parallax effect runs. 0.0 means the image will appear fixed in place, and 1.0 the image will flow at the same speed as the page content.'),
  );

  $form['parallax']['z_index'] = array(
    '#type' => 'textfield',
    '#title' => t('Z-Index'),
    '#default_value' => (isset($settings['z_index'])) ? $settings['z_index'] : '',
    '#description' => t('The z-index value of the fixed-position elements. By default these will be behind everything else on the page.'),
  );

  $form['parallax']['bleed'] = array(
    '#type' => 'textfield',
    '#title' => t('Bleed'),
    '#default_value' => (isset($settings['bleed'])) ? $settings['bleed'] : '',
    '#description' => t('You can optionally set the parallax mirror element to extend a few pixels above and below the mirrored element. This can hide slow or stuttering scroll events in certain browsers.'),
  );

  $form['parallax']['ios_fix'] = array(
    '#type' => 'checkbox',
    '#title' => t('iOS Fix'),
    '#default_value' => (isset($settings['ios_fix'])) ? $settings['ios_fix'] : FALSE,
    '#description' => t('iOS devices are incompatible with this plugin. If true, this option will set the parallax image as a static, centered background image whenever it detects an iOS user agent. Disable this if you wish to implement your own graceful degradation.'),
  );

  $form['parallax']['android_fix'] = array(
    '#type' => 'checkbox',
    '#title' => t('Android Fix'),
    '#default_value' => (isset($settings['android_fix'])) ? $settings['android_fix'] : FALSE,
    '#description' => t('If true, this option will set the parallax image as a static, centered background image whenever it detects an Android user agent. Disable this if you wish to enable the parallax scrolling effect on Android devices.'),
  );

  return $form;
}

/**
 * Region settings form callback.
 */
function radix_full_width_flexible_region_settings_form($settings) {
  $form = array();

  // Settings: Dimensions

  $form['size'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dimensions'),
    '#parents' => array('settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['size']['min_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum height'),
    '#default_value' => isset($settings['min_height']) ? $settings['min_height'] : '0',
    '#field_suffix' => 'px',
  );

  $form['size']['full_width_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Full-width region'),
    '#description' => t('Expands the panel region to the full width of the viewport, allowing content to render outside the grid system. <em>Note: This will only work if the theme and layout support and are configured to render full-width content.</em>'),
    '#default_value' => isset($settings['full_width_content']) ? $settings['full_width_content'] : FALSE,
  );

  $form['size']['padding'] = array(
    '#type' => 'fieldset',
    '#title' => t('Padding'),
    '#parents' => array('settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $padding_types = array(
    'padding_top' => t('Padding top'),
    'padding_left' => t('Padding left'),
    'padding_bottom' => t('Padding bottom'),
    'padding_right' => t('Padding right'),
  );
  foreach ($padding_types as $padding_type => $padding_label) {
    $form['size']['padding'][$padding_type] = array(
      '#type' => 'textfield',
      '#title' => $padding_label,
      '#default_value' => isset($settings[$padding_type]) ? $settings[$padding_type] : '0',
      '#field_suffix' => 'px',
    );
  }

  // Settings: Parallax

  $form['parallax'] = array(
    '#type' => 'fieldset',
    '#title' => t('Parallax'),
    '#parents' => array('settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['parallax']['parallax_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable parallax background on region'),
    '#default_value' => isset($settings['parallax_content']) ? $settings['parallax_content'] : FALSE,
  );

  $form['parallax']['image_src'] = array(
    '#type' => 'textfield',
    '#title' => t('Background image URI'),
    '#default_value' => (isset($settings['image_src'])) ? $settings['image_src'] : '',
    '#description' => t('You must provide a path to the image you wish to apply to the parallax effect.'),
    // Set this element to be required if the above parallax_content checkbox is checked.
    '#states' => array(
      // Action to take.
      'required' => array(
        ':input[name="settings[parallax_content]"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['parallax']['natural_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Natural width'),
    '#default_value' => (isset($settings['natural_width'])) ? $settings['natural_width'] : '',
    '#description' => t('You can provide the natural width and natural height of an image to speed up loading and reduce error when determining the correct aspect ratio of the image.'),
    '#field_suffix' => 'px',
  );

  $form['parallax']['natural_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Natural height'),
    '#default_value' => (isset($settings['natural_height'])) ? $settings['natural_height'] : '',
    '#description' => t('You can provide the natural width and natural height of an image to speed up loading and reduce error when determining the correct aspect ratio of the image.'),
    '#field_suffix' => 'px',
  );

  $form['parallax']['position_x'] = array(
    '#type' => 'textfield',
    '#title' => t('Position X'),
    '#default_value' => (isset($settings['position_x'])) ? $settings['position_x'] : '',
    '#description' => t('This is analogous to the background-position css property. Specify coordinates as top, bottom, right, left, center, or pixel values (e.g. -10px 0px). The parallax image will be positioned as close to these values as possible while still covering the target element.'),
  );

  $form['parallax']['position_y'] = array(
    '#type' => 'textfield',
    '#title' => t('Position Y'),
    '#default_value' => (isset($settings['position_y'])) ? $settings['position_y'] : '',
    '#description' => t('This is analogous to the background-position css property. Specify coordinates as top, bottom, right, left, center, or pixel values (e.g. -10px 0px). The parallax image will be positioned as close to these values as possible while still covering the target element.'),
  );

  $form['parallax']['speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Speed'),
    '#default_value' => (isset($settings['speed'])) ? $settings['speed'] : '',
    '#description' => t('The speed at which the parallax effect runs. 0.0 means the image will appear fixed in place, and 1.0 the image will flow at the same speed as the page content.'),
  );

  $form['parallax']['z_index'] = array(
    '#type' => 'textfield',
    '#title' => t('Z-Index'),
    '#default_value' => (isset($settings['z_index'])) ? $settings['z_index'] : '',
    '#description' => t('The z-index value of the fixed-position elements. By default these will be behind everything else on the page.'),
  );

  $form['parallax']['bleed'] = array(
    '#type' => 'textfield',
    '#title' => t('Bleed'),
    '#default_value' => (isset($settings['bleed'])) ? $settings['bleed'] : '',
    '#description' => t('You can optionally set the parallax mirror element to extend a few pixels above and below the mirrored element. This can hide slow or stuttering scroll events in certain browsers.'),
  );

  $form['parallax']['ios_fix'] = array(
    '#type' => 'checkbox',
    '#title' => t('iOS Fix'),
    '#default_value' => (isset($settings['ios_fix'])) ? $settings['ios_fix'] : FALSE,
    '#description' => t('iOS devices are incompatible with this plugin. If true, this option will set the parallax image as a static, centered background image whenever it detects an iOS user agent. Disable this if you wish to implement your own graceful degradation.'),
  );

  $form['parallax']['android_fix'] = array(
    '#type' => 'checkbox',
    '#title' => t('Android Fix'),
    '#default_value' => (isset($settings['android_fix'])) ? $settings['android_fix'] : FALSE,
    '#description' => t('If true, this option will set the parallax image as a static, centered background image whenever it detects an Android user agent. Disable this if you wish to enable the parallax scrolling effect on Android devices.'),
  );

  return $form;
}

/**
 * Settings validate callback.
 */
function radix_full_width_flexible_settings_validate($form, $settings) {

  // Check all the integer fields.
  $integer_positive_fields = array(
    'padding_top',
    'padding_left',
    'padding_bottom',
    'padding_right',
    'natural_width',
    'natural_height',
    'position_x',
    'position_y',
    'bleed',
  );
  $integer_fields = array(
    'z_index',
  );
  $numeric_positive_fields = array(
    'speed',
  );

  foreach ($integer_positive_fields as $integer_positive_field) {
    if ((int)$settings[$integer_positive_field] != $settings[$integer_positive_field] || $settings[$integer_positive_field] < 0) {
      form_set_error('settings][' . $integer_positive_field, t('Must be a positive, whole number.'));
    }
  }

  foreach ($integer_fields as $integer_field) {
    if ((int)$settings[$integer_field] != $settings[$integer_field] || $settings[$integer_field] < 0) {
      form_set_error('settings][' . $integer_field, t('Must be a positive, whole number.'));
    }
  }

  foreach ($numeric_positive_fields as $numeric_positive_field) {
    if (is_numeric($settings[$numeric_positive_field]) && $settings[$numeric_positive_field] < 0) {
      form_set_error('settings][' . $numeric_positive_field, t('Must be a positive, whole number.'));
    }
  }

}

/**
 * Generate CSS classes or attributes for the given settings.
 */
function _radix_full_width_flexible_build_attributes($settings, $type) {
  $class = array($type . '-radix-full-width-flexible');
  $style = array();
  $data = array();

  // Dimension settings.
  if (!empty($settings['min_height'])) {
    $style['min-height'] = $settings['min_height'] . 'px';
  }
  if (!empty($settings['padding_top'])) {
    $style['padding-top'] = $settings['padding_top'] . 'px';
  }
  if (!empty($settings['padding_left'])) {
    $style['padding-left'] = $settings['padding_left'] . 'px';
  }
  if (!empty($settings['padding_bottom'])) {
    $style['padding-bottom'] = $settings['padding_bottom'] . 'px';
  }
  if (!empty($settings['padding_right'])) {
    $style['padding-right'] = $settings['padding_right'] . 'px';
  }

  // Parallax settings.
  if (!empty($settings['parallax_content']) && !empty($settings['image_src'])) {
    if (!empty($settings['image_src'])) {
      $data['data-image-src'] = $settings['image_src'];
    }
    if (!empty($settings['natural_width'])) {
      $data['data-natural-width'] = $settings['natural_width'];
    }
    if (!empty($settings['natural_height'])) {
      $data['data-natural-height'] = $settings['natural_height'];
    }
    if (!empty($settings['position_x'])) {
      $data['data-position-x'] = $settings['position_x'];
    }
    if (!empty($settings['position_y'])) {
      $data['data-position-y'] = $settings['position_y'];
    }
    if (!empty($settings['speed'])) {
      $data['data-speed'] = $settings['speed'];
    }
    if (!empty($settings['z_index'])) {
      $data['data-z-index'] = $settings['z_index'];
    }
    if (!empty($settings['bleed'])) {
      $data['data-bleed'] = $settings['bleed'];
    }
    if (!empty($settings['ios_fix'])) {
      $data['data-ios-fix'] = $settings['ios_fix'];
    }
    if (!empty($settings['android_fix'])) {
      $data['data-android-fix'] = $settings['android_fix'];
    }
  }

  // Convert into an array.
  $attributes = array();
  if (!empty($class)) {
    $attributes['class'] = $class;
  }
  if (!empty($style)) {
    $attributes['style'] = $style;
  }
  if (!empty($data)) {
    $attributes['data'] = $data;
  }
  return $attributes;
}

/**
 * Collapse an associative style array into a simple array.
 */
function _radix_full_width_flexible_style_array($style) {
  $style_array = array();
  foreach ($style as $style_name => $style_value) {
     $style_array[] = "$style_name: $style_value;";
  }
  return $style_array;
}

/**
 * Take an attributes array and generate the string.
 */
function _radix_full_width_flexible_render_attributes($attributes) {
  // Collapse style into an array.
  if (!empty($attributes['style'])) {
    $attributes['style'] = _radix_full_width_flexible_style_array($attributes['style']);
  }
  if (!empty($attributes['data'])) {
    $data_attributes = $attributes['data'];
    unset($attributes['data']);
    $attributes = array_merge($attributes, $data_attributes);
  }
  return drupal_attributes($attributes);
}

/**
 * Theme function for the pane style.
 */
function theme_radix_full_width_flexible_render_pane($vars) {
  $content = $vars['content'];
  $pane = $vars['pane'];
  $display = $vars['display'];
  $plugin = $vars['style'];
  $settings = $vars['settings'];

  $attributes = _radix_full_width_flexible_build_attributes($settings, 'pane');
  if (!empty($attributes['class'])) {
    $content->css_class = (!empty($content->css_class) ? $content->css_class . ' ' : '') . implode(' ', $attributes['class']);
  }
  if (!empty($attributes['style'])) {
    $content->style_attributes = implode(' ', _radix_full_width_flexible_style_array($attributes['style']));
  }
  
  // if the style is gone or has no theme of its own, just display the output.
  return theme('panels_pane', array('content' => $content, 'pane' => $pane, 'display' => $display));
}

/**
 * Theme function for the region style.
 */
function theme_radix_full_width_flexible_render_region($vars) {
  $display = $vars['display'];
  $region_id = $vars['region_id'];
  $panes = $vars['panes'];
  $settings = $vars['settings'];

  if (!empty($settings['parallax_content']) && !empty($settings['image_src'])) {
    drupal_add_js(libraries_get_path('parallaxjs') . '/parallax.min.js');
    drupal_add_js(drupal_get_path('module', 'radix_full_width')
        . '/plugins/styles/radix_full_width_flexible/parallax.js');
  }

  $add_container = $vars['full_width'] && empty($settings['full_width_content']) && radix_full_width_is_region_full_width($vars['region_id'], $vars['display']->layout);

  $attributes = _radix_full_width_flexible_build_attributes($settings, 'region');

  // If adding a container, move some attributes to the container.
  $container_attributes = array();
  if ($add_container && !empty($attributes['style'])) {
    foreach ($attributes['style'] as $style_name => $style_value) {
      // Move all the padding attributes.
      if (strpos($style_name, 'padding') === 0) {
        $container_attributes['style'][$style_name] = $style_value;
        unset($attributes['style'][$style_name]);
      }
    }
  }

  // Add our default classes.
  if (empty($attributes['class'])) {
    $attributes['class'] = array();
  }
  $attributes['class'] = array_merge(
    array('region', 'region-' . $vars['region_id']),
    $attributes['class']);

  if (!empty($settings['parallax_content']) && !empty($settings['image_src'])) {
    $attributes['class'] = array_merge(array('panels-parallax-region'), $attributes['class']);
  }

  $output = '<div' . _radix_full_width_flexible_render_attributes($attributes) . '>';

  if ($add_container) {
    $output .= '<div class="container">';
    if (!empty($container_attributes)) {
      $output .= '<div class="container-inner"' . _radix_full_width_flexible_render_attributes($container_attributes) . '>';
    }
  }
  $output .= implode('<div class="panel-separator"></div>', $panes);
  if ($add_container) {
    $output .= '</div>';
    if (!empty($container_attributes)) {
      $output .= '</div>';
    }
  }

  $output .= '</div>';

  return $output;
}
