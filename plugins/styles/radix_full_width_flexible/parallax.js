(function ($) {
  'use strict';
  Drupal.behaviors.radix_full_width_parallax = {
    attach: function (context, settings) {
      $('.panels-parallax-region').parallax();
    }
  };
})(jQuery);
