core = 7.x
api = 2

; Parallax.js v1.4.2
libraries[parallaxjs][download][type] = file
libraries[parallaxjs][download][url] = https://github.com/pixelcog/parallax.js/archive/v1.4.2.tar.gz
